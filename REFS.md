
# SUPERCOLLIDER

- SC Synthdef file: https://depts.washington.edu/dxscdoc/Help/Reference/Synth-Definition-File-Format.html
- SC Server API: https://depts.washington.edu/dxscdoc/Help/Reference/Server-Command-Reference.html
- SC Non Real-Time: https://depts.washington.edu/dxscdoc/Help/Guides/Non-Realtime-Synthesis.html
- SC Score (NRT): http://doc.sccode.org/Classes/Score.html
- SC CTK (Composer tool kit NRT): https://depts.washington.edu/dxscdoc/Help/Overviews/Ctk.html

- SC Use of score & NRT: https://madskjeldgaard.dk/posts/2019-08-05-supercollider-how-to-render-patterns-as-sound-files-using-nrt

# INTERFACE & ASCII

- ASCII UI: https://external-preview.redd.it/XnRy8RnMy2F5FTHD_IFlNe-siHKUotmR6XRbblFYIis.png?auto=webp&s=7c9ff27743642135ec3a3d1716f5ab2f43929441
- ASCII UI: https://www.gridsagegames.com/cogmind/game/ui_preview/cogmind_UI_2880x1800_ASCII_smallcaps.png
- ASCII UI: https://i.pinimg.com/originals/26/8b/09/268b092f00055ae9dded11b6f629fc85.jpg

- DAW BITWIG: https://s11234.pcdn.co/wp-content/uploads/2014/04/Bitwig_MixPanel.png
- DAW ABLETON: https://milestree851.weebly.com/uploads/1/2/7/2/127278400/309828424.jpg
- DAW FLSTUDTIO: https://i.ytimg.com/vi/Y7d3lnU1K4o/maxresdefault.jpg
