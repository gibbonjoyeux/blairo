################################################################################
## SOURCES #####################################################################
################################################################################

SRCS		= 	$(wildcard src/*.c) \
				$(wildcard src/*/*.c)
OBJS		= 	$(SRCS:src/%.c=obj/%.o)
INCS		=	$(wildcard inc/*.h)

#$(info    	SOURCES -> $(SRCS))
#$(info    	OBJECTS -> $(OBJS))

NAME		= blairo
CC			= gcc
INCLUDES	= 	-I inc/ \
				-I lib/c-basics/inc/ \
				-I lib/c-string/inc/ \
				-I lib/c-linked-list/inc/
LIB			= 	-I lib/c-basics/inc/c-basics.a \
				-I lib/c-string/inc/c-string.a \
				-I lib/c-linked-list/inc/c-linked-list.a
FLAGS		= -Wall -Wextra -Werror -g

ifdef DEBUG
FLAGS		=
endif

################################################################################
## RULES #######################################################################
################################################################################

##############################
## RULE BINARY ###############
##############################
all: $(NAME)

run: all
	@./$(NAME)

##############################
### RULE LIBRARY #############
##############################
$(NAME): $(OBJS)
	@make -C lib/c-basics
	@make -C lib/c-string
	@make -C lib/c-linked-list
	@$(CC) $(FLAGS) $(OBJS) $(LIB) -o $(NAME)

##############################
### RULE SOURCES #############
##############################
obj/%.o: src/%.c Makefile $(INCS)
	@mkdir -p $(dir $(@))
	@echo $(<:src/%=%)
	@$(CC) $(FLAGS) $(INCLUDES) -o $(@) -c $<

##############################
### RULE CLEANING ############
##############################
clean:
	@rm -rf $(OBJS)

fclean: clean
	@rm -rf $(NAME)

##############################
### RULE RESTART #############
##############################
re: fclean
	@make
